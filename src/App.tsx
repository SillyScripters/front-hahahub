
import "./App.css"
import '@fontsource/roboto/300.css'
import '@fontsource/roboto/400.css'
import '@fontsource/roboto/500.css'
import '@fontsource/roboto/700.css'
import { Suspense } from "react"
import { LinearProgress } from "@mui/material"
import { MainLayout } from "@/components/layout/main-layout/main-layout"

import AppRoutes from "@/pages/app-routes"

const App = () => {
  return (
    <MainLayout>
      <Suspense
        fallback={
          <LinearProgress></LinearProgress>
        }
      >
        <AppRoutes></AppRoutes>
      </Suspense>
    </MainLayout>
  )
}

export default App
