import { Footer, Nav } from "@/components/layout/navigation"
import { Box, Button, Container, CssBaseline, ThemeProvider, Typography, createTheme } from "@mui/material"
import { PropsWithChildren, useState } from "react"
import { Routes } from "react-router-dom";

interface Props {

}

const defaultTheme = createTheme();

export const MainLayout = ({ children }: PropsWithChildren<Props>) => {
    return <ThemeProvider theme={defaultTheme}>
        <Box
            sx={{
                display: 'flex',
                flexDirection: 'column',
                minHeight: '100vh',
            }}
        >
            <CssBaseline />

            <Nav></Nav>
            <br></br>

            {children}

            <Footer></Footer>
        </Box>
    </ThemeProvider>
}