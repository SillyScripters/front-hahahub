import { useLocation, useNavigate } from "react-router-dom";
import goBackIcon from "@/assets/return-back-svgrepo-com.svg"
import { Box, Button } from "@mui/material";


const GoBackButton = () => {
    const navigate = useNavigate();
    const location = useLocation();

    const goBack = () => {
        navigate(-1);
    }

    return location.key !== "default" ? <Button onClick={goBack}>
        <img src={goBackIcon} width={50}></img>
    </Button> : <></>
}

export default GoBackButton