export const API_URL = import.meta.env.VITE_REACT_API_URL as string;
export const DEFAULT_PAGE_SIZE = import.meta.env.VITE_REACT_DEFAULT_PAGE_SIZE as number;