import { axios } from "@/lib/axios"
import { PaginatedList } from "@/types";
import { Comment } from "@/types/feed"
import { createSearchParamsFromObject } from "@/utils/create-search-params";

export type getCommentsSearchParams = {
    pageNumber: number;
    pageSize: number;
}

export const getPostCommentsPaginatedList = (
    id: string, 
    searchParams: getCommentsSearchParams): Promise<{data: PaginatedList<Comment>}> => {
    return axios.post(`/post/${id}/comments`, {}, {
        params: createSearchParamsFromObject(searchParams)
    })
}