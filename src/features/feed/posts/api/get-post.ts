import { axios } from "@/lib/axios"
import { Post } from "@/types/feed"

export const getPost = (id: string): Promise<{data: Post}> => {
    return axios.get(`/post/${id}`)
}