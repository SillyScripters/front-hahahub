import { axios } from "@/lib/axios"
import { PaginatedList } from "@/types";
import { Post } from "@/types/feed"
import { createSearchParamsFromObject } from "@/utils/create-search-params";

export type getPostsSearchParams = {
    pageNumber: number;
    pageSize: number;
    startId?: number;
    userId?: number;
}

export const getPostsPaginatedList = (searchParams: getPostsSearchParams, onlySubscriptions: boolean): Promise<{ data: PaginatedList<Post> }> => {
    return axios.post(onlySubscriptions ? `/post/subscriptions-page` : `/post/page`, {}, {
        params: createSearchParamsFromObject(searchParams)
    })
}