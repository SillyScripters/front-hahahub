import { axios } from "@/lib/axios";
import { Post } from "@/types/feed";
import { createSearchParamsFromObject } from "@/utils/create-search-params";

export type updatePostsSearchParams = {
    lastId: number;
}

export const updatePostsPaginatedList = (searchParams: updatePostsSearchParams, onlySubscriptions: boolean): Promise<{data: Post[]}> => {
    return axios.post(onlySubscriptions ? `/post/subscriptions-page/update` : `/post/page/update`, {}, {
        params: createSearchParamsFromObject(searchParams)
    })
}