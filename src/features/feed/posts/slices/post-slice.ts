import { PaginatedList } from "@/types";
import { Comment, Post } from "@/types/feed";
import { PayloadAction, createSlice, current } from "@reduxjs/toolkit";

interface PostState {
    post?: Post;
    commentsPage?: PaginatedList<Comment>;
    pageNumber: number;
}

const initialState: PostState = {
    pageNumber: 1
};

export const postSlice = createSlice({
    name: 'post',
    initialState,
    reducers: {
        setPost: (state: PostState, action: PayloadAction<Post>) => {
            state.post = action.payload;
        },
        setCommentsPage: (state: PostState, action: PayloadAction<PaginatedList<Comment>>) => {
            state.commentsPage = action.payload;
        },
        setPageNumber: (state: PostState, action: PayloadAction<number>) => {
            state.pageNumber = action.payload;
        },
    },
});

export const { setPost, setCommentsPage, setPageNumber } = postSlice.actions;

export default postSlice;