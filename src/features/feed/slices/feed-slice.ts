import { DEFAULT_PAGE_SIZE } from "@/config";
import { getPostsPaginatedList, updatePostsPaginatedList } from "@/features/feed/posts/api";
import { RootState } from "@/store";
import { PaginatedList } from "@/types";
import { Post } from "@/types/feed";
import { ActionReducerMapBuilder, PayloadAction, createAsyncThunk, createSlice, current } from "@reduxjs/toolkit";

interface FeedState {
    onlySubscriptions: boolean;
    postsPage?: PaginatedList<Post>;
    posts: Post[];
    startId?: number;
    subsButtonChecked: boolean;
    isLoading: boolean;
    isUpdating: boolean;
}

const initialState: FeedState = {
    onlySubscriptions: false,
    posts: [],
    subsButtonChecked: false,
    isLoading: false,
    isUpdating: false
};

const createGetPosts = (onlySubscriptions: boolean) => createAsyncThunk<PaginatedList<Post>, void, { state: RootState }>(
    onlySubscriptions
        ? "subsFeed/getPosts"
        : "feed/getPosts",
    async (_, thunkAPI) => {
        const state = onlySubscriptions
            ? thunkAPI.getState().subsFeed
            : thunkAPI.getState().feed;
        const response = await getPostsPaginatedList({
            pageNumber: (state.postsPage?.pageNumber || 0) + 1,
            pageSize: DEFAULT_PAGE_SIZE,
            startId: state.startId
        }, onlySubscriptions);
        return response.data;
    }
)

export const getPosts = createGetPosts(false);
export const getSubsPosts = createGetPosts(true);

const createUpdatePosts = (onlySubscriptions: boolean) => createAsyncThunk<Post[], void, { state: RootState }>(
    onlySubscriptions
        ? "subsFeed/updatePosts"
        : "feed/updatePosts",
    async (_, thunkAPI) => {
        const state = onlySubscriptions
            ? thunkAPI.getState().subsFeed
            : thunkAPI.getState().feed;
        const response = await updatePostsPaginatedList({ lastId: state.posts[0].id }, onlySubscriptions);
        return response.data;
    }
)

export const updatePosts = createUpdatePosts(false);
export const updateSubsPosts = createUpdatePosts(true);

const reducers = {
    setPosts: (state: FeedState, action: PayloadAction<Post[]>) => {
        state.posts = [...state.posts, ...action.payload];
    },
    setPostsPage: (state: FeedState, action: PayloadAction<PaginatedList<Post>>) => {
        state.postsPage = action.payload;
    },
    setStartId: (state: FeedState, action: PayloadAction<number | undefined>) => {
        state.startId = action.payload
    },
    setPostsToTop: (state: FeedState, action: PayloadAction<Post[]>) => {
        state.posts = [...action.payload, ...state.posts];
    },
    setSubsButtonChecked: (state: FeedState, action: PayloadAction<boolean>) => {
        state.subsButtonChecked = action.payload
    },
}

const extraReducers = (onlySubscriptions: boolean) => {
    return (builder: ActionReducerMapBuilder<FeedState>) => {
        const getPostsAction = onlySubscriptions ? getSubsPosts : getPosts
        const updatePostsAction = onlySubscriptions ? updateSubsPosts : updatePosts
        
        builder
            .addCase(getPostsAction.pending, (state: FeedState, action: PayloadAction) => {
                state.isLoading = true;
            })
            .addCase(getPostsAction.fulfilled, (state: FeedState, action: PayloadAction<PaginatedList<Post>>) => {
                state.postsPage = action.payload;
                state.posts = [...state.posts, ...action.payload.items]
                if (!state.startId) state.startId = action.payload.items[0].id;
                state.isLoading = false;
            })
            .addCase(getPostsAction.rejected, (state: FeedState, action: PayloadAction<any>) => {
                console.log(action.payload);
                state.isLoading = false;
            })
            .addCase(updatePostsAction.pending, (state: FeedState, action: PayloadAction) => {
                state.isUpdating = true;
            })
            .addCase(updatePostsAction.fulfilled, (state: FeedState, action: PayloadAction<Post[]>) => {
                state.posts = [...action.payload, ...state.posts];
                state.isUpdating = false;
            })
            .addCase(updatePostsAction.rejected, (state: FeedState, action: PayloadAction<any>) => {
                console.log(action.payload);
                state.isLoading = false;
            })
    }
}

export const feedSlice = createSlice({
    name: 'feed',
    initialState: initialState,
    reducers: {
        ...reducers
    },
    extraReducers: extraReducers(false)
});

export const subsFeedSlice = createSlice({
    name: 'subsFeed',
    initialState: initialState,
    reducers: reducers,
    extraReducers: extraReducers(true)
});


export const feedActions = feedSlice.actions;

export const subsFeedActions = subsFeedSlice.actions;
