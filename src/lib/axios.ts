import { API_URL } from "@/config";
import getTestJwt from "@/utils/get-test-jwt";
import Axios from "axios";

export const axios = Axios.create({
  baseURL: API_URL,
});

//Просто тестовый токен с NameIdentifier -1
axios.interceptors.request.use(
  (config) => {
    const token = getTestJwt();
    if (token) {
      config.headers['Authorization'] = `Bearer ${token}`;
    }
    return config;
  },
  (error) => {
    Promise.reject(error);
  }
);