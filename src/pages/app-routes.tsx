import { protectedRoutes } from "@/pages/protected-routes";
import { publicRoutes } from "@/pages/public-routes";
import { useState } from "react";
import { useRoutes } from "react-router-dom";

const AppRoutes = () => {
    const auth = useState<boolean>(true) // заменить реализацией аутентификации
    const routes = useRoutes(auth ? protectedRoutes : publicRoutes)
    
    return <>{routes}</>
}

export default AppRoutes;