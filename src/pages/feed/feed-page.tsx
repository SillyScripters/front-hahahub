import FeedScroller from "@/pages/feed/feed-scroller";
import { Box, Button, ButtonGroup, Container, Divider} from "@mui/material"
import { memo, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { AppDispatch, RootState } from "@/store"
import { feedActions } from "@/features/feed/slices/feed-slice";

const Feed = memo(() => {
    const onlySubscriptions = useSelector((gs: RootState) => gs.feed.subsButtonChecked);
    const dispatch = useDispatch<AppDispatch>();

    return <Container maxWidth="md" sx={
        {
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            overflow: "auto",
            textWrap: "wrap",
            width: "40vw"
        }
    } id="feed">
        <ButtonGroup
            variant="text"
            color="primary"
            fullWidth>
            <Button disabled={!onlySubscriptions} onClick={() => dispatch(feedActions.setSubsButtonChecked(false))}>Лента</Button>
            <Button disabled={onlySubscriptions} onClick={() => dispatch(feedActions.setSubsButtonChecked(true))}>Подписки</Button>
        </ButtonGroup>
        <Divider flexItem></Divider>

        {!onlySubscriptions ? <FeedScroller></FeedScroller> : <></>}
        {onlySubscriptions && <FeedScroller onlySubscriptions></FeedScroller>}
    </Container>
})

export default Feed