import { DEFAULT_PAGE_SIZE } from "@/config"
import { getPostsPaginatedList, updatePostsPaginatedList } from "@/features/feed/posts/api"
import { feedActions, getPosts, getSubsPosts, subsFeedActions, updatePosts, updateSubsPosts } from "@/features/feed/slices/feed-slice"
import { useEffectOnce } from "@/hooks/use-effect-once"
import ScrollBackToTopButton from "@/pages/feed/scroll-back-to-top-button"
import ShortPostCard from "@/pages/feed/short-post-card"
import { AppDispatch, RootState } from "@/store"
import { PaginatedList } from "@/types"
import { Post } from "@/types/feed"
import { Box, Button, LinearProgress } from "@mui/material"
import { memo, useCallback, useEffect, useState } from "react"
import InfiniteScroll from "react-infinite-scroll-component"
import { ConnectedProps, connect } from "react-redux"

interface OwnProps {
    onlySubscriptions?: boolean,
}

const mapStateToProps = (gs: RootState, ownProps: OwnProps) => {
    return {
        posts: ownProps.onlySubscriptions
            ? gs.subsFeed.posts
            : gs.feed.posts,
        postsPage: ownProps.onlySubscriptions
            ? gs.subsFeed.postsPage
            : gs.feed.postsPage,
        startId: ownProps.onlySubscriptions
            ? gs.subsFeed.startId
            : gs.feed.startId,
        isUpdating: ownProps.onlySubscriptions
            ? gs.subsFeed.isUpdating
            : gs.feed.isUpdating,
    }
}

const mapDispatchToProps = (dispatch: AppDispatch, ownProps: OwnProps) => {
    return {
        setPosts: (posts: Post[]) => {
            dispatch(ownProps.onlySubscriptions
                ? subsFeedActions.setPosts(posts)
                : feedActions.setPosts(posts))
        },
        setPostsPage: (postsPage: PaginatedList<Post>) => {
            dispatch(ownProps.onlySubscriptions
                ? subsFeedActions.setPostsPage(postsPage)
                : feedActions.setPostsPage(postsPage))
        },
        setStartId: (startId: number) => {
            dispatch(ownProps.onlySubscriptions
                ? subsFeedActions.setStartId(startId)
                : feedActions.setStartId(startId))
        },
        setPostsToTop: (posts: Post[]) => {
            dispatch(ownProps.onlySubscriptions
                ? subsFeedActions.setPostsToTop(posts)
                : feedActions.setPostsToTop(posts))
        },
        getPosts: () => {
            dispatch(ownProps.onlySubscriptions
                ? getSubsPosts()
                : getPosts())
        },
        updatePosts: () => {
            dispatch(ownProps.onlySubscriptions
                ? updateSubsPosts()
                : updatePosts())
        }
    }
}

const connector = connect(mapStateToProps, mapDispatchToProps)

type PropsFromRedux = ConnectedProps<typeof connector>

type FeedScrollerProps = OwnProps & PropsFromRedux

const FeedScroller = memo(({
    posts,
    postsPage,
    isUpdating,
    getPosts,
    updatePosts,
    startId,
    setPosts,
    setPostsPage,
    setStartId,
    setPostsToTop,
    onlySubscriptions = false
}: FeedScrollerProps) => {
    // const [isLoading, setLoading] = useState<boolean>(false);
    // const [isUpdating, setUpdating] = useState<boolean>(false);

    useEffectOnce(() => {
        if (posts.length === 0)
            getPosts();
    })

// #region getPosts
    // const getPosts = useCallback(() => {
    //     if (isLoading) return;
    //     setLoading(true);

    //     getPostsPaginatedList({
    //         pageNumber: (postsPage?.pageNumber || 0) + 1,
    //         pageSize: DEFAULT_PAGE_SIZE,
    //         startId: startId
    //     }, onlySubscriptions)
    //         .then(response => {
    //             if (response.data.items.length !== 0) {
    //                 setPostsPage(response.data);
    //                 setPosts(response.data.items);
    //                 if (!startId) setStartId(response.data.items[0].id);
    //             }
    //             setLoading(false);
    //         })
    //         .catch(err => {
    //             console.log(err);
    //             setLoading(false);
    //         });
    // }, [isLoading, postsPage, startId])

    // const updatePosts = useCallback(() => {
    //     if (isUpdating) return;
    //     setUpdating(true);

    //     updatePostsPaginatedList({ lastId: posts[0].id }, onlySubscriptions)
    //         .then(response => {
    //             if (response.data.length > 0)
    //                 setPostsToTop(response.data);
    //             setUpdating(false);
    //         })
    //         .catch(err => {
    //             console.log(err);
    //             setUpdating(false);
    //         });
    // }, [posts, isUpdating])
// #endregion

    return <Box>
        <ScrollBackToTopButton />

        <Button fullWidth onClick={updatePosts}>обновить</Button>

        <InfiniteScroll
            dataLength={posts.length}
            next={getPosts}
            hasMore={postsPage?.hasNextPage ?? true}
            loader={<LinearProgress key="-1" />}
        // pullDownToRefresh={false}
        // pullDownToRefreshContent={<div><LinearProgress key="-1" /></div>}
        // pullDownToRefreshThreshold={100}
        // releaseToRefreshContent={<LinearProgress key="-1" />}
        // refreshFunction={updatePosts}
        >
            {isUpdating && <LinearProgress key="-1"></LinearProgress>}
            {

                posts && posts.map((post, index) => {
                    return <ShortPostCard post={post} key={index}></ShortPostCard>
                })
            }
        </InfiniteScroll>
    </Box>
})

export default connector(FeedScroller)