
import scrollUpIcon from "@/assets/up-arrow-left-right-down-direction-svgrepo-com.svg"
import scrollDownIcon from "@/assets/down-arrow-left-right-svgrepo-com.svg"
import { useEffect, useState } from "react"
import { Button } from "@mui/material"

const buttonStyle = { height: "100vh", width: "20vh", position: "fixed", top: 0, left: 0 }

const ScrollBackToTopButton = () => {
    const [previousPosition, setPreviousPosition] = useState<number>()
    const [scrollPosition, setScrollPosition] = useState<number>(0);

    useEffect(() => {
        setScrollPosition(window.scrollY)
        window.addEventListener('scroll', handleScroll, { passive: true });

        return () => {
            window.removeEventListener('scroll', handleScroll);
        };
    }, []);

    const handleScroll = () => {
        const position = window.scrollY;
        setScrollPosition(position);
    };

    const scrollToPreviousPosition = () => {
        setPreviousPosition(undefined)
        window.scrollTo({ top: previousPosition, behavior: "smooth" })
    }

    const scrollToTop = () => {
        setPreviousPosition(scrollPosition)
        window.scrollTo({ top: 0, behavior: "smooth" })
    }

    return (scrollPosition > 1000) ?
        <Button
            onClick={scrollToTop}
            sx={buttonStyle}>
            <img src={scrollUpIcon} width={50}></img>
        </Button> : previousPosition ? <Button
            onClick={scrollToPreviousPosition}
            sx={buttonStyle}>
            <img src={scrollDownIcon} width={50}></img>
        </Button> : <></>
}

export default ScrollBackToTopButton