import { Post } from "@/types/feed"
import { Avatar, Card, CardActions, CardContent, CardHeader, CardMedia, IconButton, Typography } from "@mui/material"
import { memo } from "react"
import { Link } from "react-router-dom"
import FavoriteIcon from '@mui/icons-material/Favorite';
import smile from "@/assets/smile.jpeg"

interface ShortPostCardProps {
    post: Post
}

const ShortPostCard = memo(({ post }: ShortPostCardProps) => {
    return <Link style={{ textDecoration: "none", color: "black" }} to={`/post/${post.id}`}>
        <Card variant="outlined" sx={
            {
                marginBottom: "2vh"
            }}>
            <CardHeader
                avatar={<Avatar alt={post.user.name} />}
                title={post.user.name}>

            </CardHeader>
            <CardContent>
                <CardMedia
                    image={(post.id % 2 == 0) ? `https://random.imagecdn.app/500/300?${post.id}}` : ""} //временно для красоты
                    component="img">
                </CardMedia>
                {/* <Typography fontWeight={"bold"}>
                        {post.user.name}
                    </Typography> */}
                <Typography>
                    {post.postText}
                </Typography>
                <br></br>
                <Typography sx={{ color: "#808080", fontSize: 12 }}>
                    {`${post.category.name}: ${post?.hashTag}`}
                </Typography>
                <hr></hr>
                <Typography sx={{ color: "#808080", fontSize: 12 }}>
                    {new Date(post.created).toLocaleString()}
                </Typography>
            </CardContent>
            <CardActions disableSpacing>
                <IconButton aria-label="like">
                    <FavoriteIcon />
                </IconButton>
                {post.likesCount
                    ? <IconButton aria-label="show-likes">
                        <Typography>{post.likesCount}</Typography>
                    </IconButton>
                    : <></>
                }
            </CardActions>
        </Card>
    </Link>
})

export default ShortPostCard