import { Container, Typography } from "@mui/material"

export const NotFound = () => {
    return <Container>
        <Typography>Page not exists</Typography>
    </Container>
}