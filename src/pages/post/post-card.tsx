import { setPost } from "@/features/feed/posts/slices/post-slice"
import { AppDispatch, RootState } from "@/store"
import { Post } from "@/types/feed"
import { Avatar, Card, CardActions, CardContent, CardHeader, CardMedia, IconButton, Typography } from "@mui/material"
import { memo, useState } from "react"
import { ConnectedProps, connect } from "react-redux"
import { useParams } from "react-router-dom"
import FavoriteIcon from '@mui/icons-material/Favorite';
import smile from "@/assets/smile.jpeg"
import { useEffectOnce } from "@/hooks/use-effect-once"
import { getPost } from "@/features/feed/posts/api"

const mapStateToProps = (gs: RootState) => {
    return {
        post: gs.post.post,
    }
}

const mapDispatchToProps = (dispatch: AppDispatch) => {
    return {
        setPost: (post: Post) => { dispatch(setPost(post)) },
    }
}

const connector = connect(mapStateToProps, mapDispatchToProps)

type PropsFromRedux = ConnectedProps<typeof connector>

interface PostCardProps extends PropsFromRedux {

}

const PostCard = memo(({
    post,
    setPost
}: PostCardProps) => {
    const [isLoading, setLoading] = useState<boolean>(false);

    const { id } = useParams();

    useEffectOnce(() => {
        // if (!post)
        getPost(id!)
            .then(response => {
                setPost(response.data);
            })
            .catch(err => {
                console.log(err);
            });
    })

    return <>
        {post &&
            <Card variant="outlined" sx={{ width: "40vw" }}>
                <CardContent>
                    <CardHeader
                        avatar={<Avatar alt={post.user.name} />}
                        title={post.user.name}>

                    </CardHeader>
                    <CardMedia
                        image={(post.id % 2 == 0) ? `https://random.imagecdn.app/500/300?${post.id}}` : ""} //временно для красоты
                        component="img">
                    </CardMedia>
                    {/* <Typography fontWeight={"bold"}>
                        {post.user.name}
                    </Typography> */}
                    <Typography>
                        {post.postText}
                    </Typography>
                    <br></br>
                    <Typography sx={{ color: "#808080", fontSize: 12 }}>
                        {`${post.category.name}: ${post?.hashTag}`}
                    </Typography>
                    <hr></hr>
                    <Typography sx={{ color: "#808080", fontSize: 12 }}>
                        {new Date(post.created).toLocaleString()}
                    </Typography>
                </CardContent>
                <CardActions disableSpacing>
                    <IconButton aria-label="like">
                        <FavoriteIcon />
                    </IconButton>
                    {post.likesCount
                        ? <IconButton aria-label="show-likes">
                            <Typography>{post.likesCount}</Typography>
                        </IconButton>
                        : <></>
                    }
                </CardActions>
            </Card>
        }
    </>
})

export default connector(PostCard)