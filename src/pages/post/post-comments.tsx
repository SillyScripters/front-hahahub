import { setCommentsPage, setPageNumber, setPost } from "@/features/feed/posts/slices/post-slice"
import { AppDispatch, RootState } from "@/store"
import { Comment } from "@/types/feed"
import { Card, CardContent, LinearProgress, Pagination, Typography } from "@mui/material"
import { ChangeEvent, memo, useEffect, useState } from "react"
import { ConnectedProps, connect } from "react-redux"
import { useParams } from "react-router-dom"
import { PaginatedList } from "@/types"
import { getPostCommentsPaginatedList } from "@/features/feed/posts/api/get-post-comments-paginated-list"
import { DEFAULT_PAGE_SIZE } from "@/config"

const mapStateToProps = (gs: RootState) => {
    return {
        commentsPage: gs.post.commentsPage,
        pageNumber: gs.post.pageNumber,
    }
}

const mapDispatchToProps = (dispatch: AppDispatch) => {
    return {
        setCommentsPage: (commentsPage: PaginatedList<Comment>) => { dispatch(setCommentsPage(commentsPage)) },
        setPageNumber: (pageNumber: number) => { dispatch(setPageNumber(pageNumber)) }
    }
}

const connector = connect(mapStateToProps, mapDispatchToProps)

type PropsFromRedux = ConnectedProps<typeof connector>

interface PostCardProps extends PropsFromRedux {

}

const PostComments = memo(({
    commentsPage,
    pageNumber,
    setCommentsPage,
    setPageNumber
}: PostCardProps) => {
    const [isLoading, setLoading] = useState<boolean>(false);

    const { id } = useParams();

    useEffect(() => {
        getComments();
    }, [pageNumber])

    const handlePagination = (event: ChangeEvent<unknown>, value: number) => {
        setPageNumber(value);
    }

    const getComments = () => {
        if (isLoading) return;
        setLoading(true);

        getPostCommentsPaginatedList(id!, {
            pageNumber: pageNumber,
            pageSize: DEFAULT_PAGE_SIZE
        })
            .then(response => {
                setCommentsPage(response.data);
                setLoading(false);
            })
            .catch(err => {
                console.log(err);
                setLoading(false)
            })
    }

    return <>
        {commentsPage ? <>
            <Pagination
                page={pageNumber}
                count={commentsPage.totalPages}
                onChange={handlePagination}
                showFirstButton
                showLastButton />
            {commentsPage.items.map((comment, index) => {
                return <Card key={index} variant="outlined" sx={{ margin: 1, width: "30vw" }}>
                    <CardContent>
                        <Typography fontWeight={"bold"}>
                            {comment.user.name}
                        </Typography>
                        <Typography>{comment.commentText}</Typography>
                        <hr></hr>
                        <Typography sx={{ color: "#808080", fontSize: 12 }}>
                            {new Date(comment.created).toLocaleString()}
                        </Typography>
                    </CardContent>
                </Card>
            })}
            <Pagination
                page={pageNumber}
                count={commentsPage.totalPages}
                onChange={handlePagination}
                showFirstButton
                showLastButton />
        </> : <LinearProgress></LinearProgress>}
    </>
})

export default connector(PostComments)