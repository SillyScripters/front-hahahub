import { Box, Container } from "@mui/material"
import PostComments from "@/pages/post/post-comments";
import PostCard from "@/pages/post/post-card";
import { memo } from "react";
import GoBackButton from "@/components/shared/go-back-button/go-back-button";

const PostPage = memo(() => {
    return <Container maxWidth="lg" sx={
        {
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            overflow: "auto",
            textWrap: "wrap"
        }
    } id="post">
        <PostCard></PostCard>

        <PostComments></PostComments>

        <Box sx={{ display: "flex", justifyContent: "flex-end", width: "30vw" }}>
            <GoBackButton />
        </Box>
    </Container>
})

export default PostPage