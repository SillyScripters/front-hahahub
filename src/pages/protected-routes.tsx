import { NotFound } from "@/pages/not-found/not-found"
import { lazy } from "react"

const FeedPage = lazy(() => import("@/pages/feed/feed-page"))
const PostPage = lazy(() => import("@/pages/post/post-page"))

export const protectedRoutes = [
  { path: "/feed", element: <FeedPage /> },
  { path: "/post/:id", element: <PostPage />},
  { path: "*", element: <NotFound />}
]