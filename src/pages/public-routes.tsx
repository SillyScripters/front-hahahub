import { NotFound } from "@/pages/not-found/not-found";

export const publicRoutes = [
    {path: "/auth", element: <NotFound />}
]