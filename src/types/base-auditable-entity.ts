import { BaseEntity } from "@/types";
import { z } from "zod";

export type BaseAuditableEntity = {
    created: string;
    lastModified: string; 
} & BaseEntity

// export const BaseAuditableEntitySchema = z.object(
//     {
//         created: z.coerce.date(),
//         lastModified: z.coerce.date()
//     }
// ).catchall(z.unknown())

// export const BaseAuditableEntityArraySchema = z.array(BaseAuditableEntitySchema)

// export const BaseAuditableEntityPaginatedListSchema = z.object({
//     items: BaseAuditableEntityArraySchema
// }).catchall(z.unknown())

// export type BaseAuditableEntity = z.infer<typeof BaseAuditableEntitySchema> & BaseEntity
