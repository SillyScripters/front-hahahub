import { BaseAuditableEntity } from "@/types"

export type Category = {
    name: string
} & BaseAuditableEntity