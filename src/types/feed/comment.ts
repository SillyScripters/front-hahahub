import { BaseAuditableEntity } from "@/types";
import { User } from "@/types/users";

export type Comment = {
    commentText: string;
    user: User;
} & BaseAuditableEntity