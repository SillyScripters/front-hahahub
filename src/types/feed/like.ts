import { BaseEntity } from "@/types";
import { User } from "@/types/users";

export type Like = {
    user: User;
} & BaseEntity