import { BaseAuditableEntity } from "@/types";
import { Category } from "@/types/feed";
import { User } from "@/types/users";

export type Post = {
    postText: string;
    imageUrl: string;
    hashTag: string;
    user: User;
    category: Category;
    likesCount: number;
} & BaseAuditableEntity