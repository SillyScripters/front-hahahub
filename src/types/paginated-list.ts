import { BaseEntity } from "@/types";

export type PaginatedList<T extends BaseEntity> = {
    items: T[];
    pageNumber: number;
    totalPages: number;
    totalCount: number;
    hasPreviousPage: boolean;
    hasNextPage: boolean;
}
