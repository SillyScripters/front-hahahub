import { BaseAuditableEntity } from "@/types";

export type Subscription = {
    userId: number;
} & BaseAuditableEntity