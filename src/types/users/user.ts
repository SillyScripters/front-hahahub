import { BaseAuditableEntity } from "@/types";

export type User = {
    name: string;
} & BaseAuditableEntity