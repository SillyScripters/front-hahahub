import { isNil } from "lodash";
import { createSearchParams } from "react-router-dom";

export const createSearchParamsFromObject = (params: any) => {
    Object.keys(params).forEach(k => {
        if (!isNil(params[k]))
            params[k] = '' + params[k];
        else
            delete params[k]
    })
    return createSearchParams(params);
}